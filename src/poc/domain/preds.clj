(ns poc.domain.preds
  (:require [clojure.string :as str]
            [cadastro-de-pessoa.cnpj :as cnpj]
            [cadastro-de-pessoa.cpf :as cpf]))

(defn cpf? [s]
  (boolean (when s (cpf/valid? s))))

(defn cnpj? [s]
  (boolean (when s (cnpj/valid? s))))

(defn cpf-or-cnpj? [s]
  (or (cpf? s)
      (cnpj? s)))

(defn lat-long-size?
  "Validates if the collection with the point values is right (with only two values)
   Ex:
    (lat-long-size? [10 10]) => true
    (lat-long-size? [10]) => false"
  [coll]
  (= (count coll) 2))

(defn in-latitude-range?
  "Validates if the collection first value is between latitude limit range (-90 to 90)
   Ex:
    (in-latitude-range? [90 100]) => true
    (in-latitude-range? [-91 180]) => false"
  [v]
  (and (<= v 90)
       (>= v -90)))

(defn in-longitude-range?
  "Validates if the collection second value is between longitude limit range (-180 to 180)
  Ex:
    (in-longitude-range? [90 180]) => true
    (in-longitude-range? [90 -181]) => false"
  [v]
  (and (<= v 180)
       (>= v -180)))

(defn non-blank?
  "Validates if the input string is not empty
  Ex:
    (non-blank? \"foo\") => true
    (non-blank? \"\") => false"
  [s]
  (not (str/blank? s)))
