(ns poc.domain.partner
  (:require [malli.core :as m]
            [malli.error :as me]
            [poc.domain.gen :as g]
            [poc.domain.preds :as pred]))

(def latitude
  [:and
   number?
   [:fn {:error/message "The latitude coordinate is out of range"}
    pred/in-latitude-range?]])

(def longitude
  [:and
   number?
   [:fn {:error/message "The longitude coordinate is out of range"}
    pred/in-longitude-range?]])

(def primitives-registry
  {:non-blank [:and string? [:fn {:error/message "Empty or blank value is not allowed"}
                             pred/non-blank?]]
   :latitude latitude

   :longitude longitude

   :lat-long [:and
              [:fn {:error/message "The coordinates must have only 2 elements (lat & long)"}
               pred/lat-long-size?]
              [:tuple
               {:gen/gen (g/gen-lat-long)}
               :latitude
               :longitude]]

   :point [:map
           [:type [:enum "Point"]]
           [:coordinates :lat-long]]})

(def multipart-geometry
  {:multipoint [:map
                [:type [:enum "MultiPolygon"]]
                [:coordinates
                 [:vector
                  [:vector
                   [:vector
                    :lat-long]]]]]})

(def registry
  (merge
   (m/class-schemas)
   (m/comparator-schemas)
   (m/base-schemas)
   (m/sequence-schemas)
   (m/type-schemas)
   (m/predicate-schemas)
   primitives-registry
   multipart-geometry))

(def partner
  [:map
   [:id :non-blank]
   [:tradingName :non-blank]
   [:ownerName :non-blank]
   [:document {:gen/gen (g/gen-cnpj-cpf)}
    [:fn {:error/message "Invalid cpf/cnpj."}
     pred/cpf-or-cnpj?]]
   [:coverageArea :multipoint]
   [:address :point]])

(def reg {:registry registry})

(defn validate [schema v]
  (m/validate schema v reg))

(defn humanize [schema v]
  (-> (m/explain schema
                 v
                 reg)
      (me/humanize)))
