(ns poc.domain.id-generator
  (:require [buddy.core.hash :as buddy.hash])
  (:import (java.lang String)
           (org.apache.commons.codec.binary Base32)))

(def ^Base32 b32
  ;; Use 0 as the padding character (instead of =) to keep things url safe.
  (Base32. 0 nil false (byte \0)))

(defn- bytes->base32-str [^bytes bytes]
  (String. (.encode b32 bytes)))

(defn- ripemd160-base32 [s]
  (bytes->base32-str (buddy.hash/ripemd160 s)))

(defn- partner-id [{:keys [document address]}]
  (ripemd160-base32 (str "partner"
                         document
                         (:coordinates address))))

(defn id? [s]
  (boolean (when (string? s)
             (re-find #"^(?:[A-Z]|[2-7]|0){32}$" s))))

(defn add-id [partner]
  (assoc partner :id (partner-id partner)))
