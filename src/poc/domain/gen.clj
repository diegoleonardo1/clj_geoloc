(ns poc.domain.gen
  (:require [clojure.spec.gen.alpha :as gen]
            [cadastro-de-pessoa.cnpj :as cnpj]
            [cadastro-de-pessoa.cpf :as cpf]))

(defn gen-latitude []
  (gen/double* {:infinite? false
                :NaN? false
                :min -90
                :max 90}))

(defn gen-longitude []
  (gen/double* {:infinite? false
                :NaN? false
                :min -180.00
                :max 180.00}))

(defn gen-lat-long []
  (gen/tuple (gen-latitude) (gen-longitude)))

(defn gen-cnpj []
  (gen/fmap (fn [_] (cnpj/gen)) (gen/uuid)))

(defn gen-cpf []
  (gen/fmap (fn [_] (cpf/gen)) (gen/uuid)))

(defn gen-cnpj-cpf []
  (gen/one-of [(gen-cnpj)
               (gen-cpf)]))
