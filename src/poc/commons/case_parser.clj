(ns poc.commons.case-parser
  (:require [clojure.set :as set]
            [camel-snake-kebab.core :as csk]))

(defn- style [case-style]
  (cond
    (= case-style :snake-case) csk/->snake_case_keyword
    (= case-style :kebab-case) csk/->kebab-case-keyword
    (= case-style :pascal-case) csk/->PascalCaseKeyword
    :else csk/->camelCaseKeyword))

(defn to-case-style [param case-style]
  "Parses the param (a collection or simple map) to case-style indicated by keyword (:camel-case (default), :snake-case, :kebab-case, :pascal-case)
   Ex:
     (to-case-style {:first-name \"Jhon\" :last-name \"Doo\"} :camel-case) => {:firstName \"John\" :lastName \"Doo\"}
     (to-case-style [{:first-name \"Jhon\" :last-name \"Doo\"}] :camel-case) => ({:firstName \"John\" :lastName \"Doo\"})"
  (if (map? param)
    (->> (map #(conj {% ((style case-style) %)})
              (keys param))
         (reduce conj)
         (set/rename-keys param))
    (map #(to-case-style % case-style) param)))
