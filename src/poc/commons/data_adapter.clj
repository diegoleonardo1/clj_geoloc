(ns poc.commons.data-adapter
  (:require [clojure.set :as set]
            [poc.commons.case-parser :as cp]
            [clj-postgresql.spatial :as st]))

(defn partner->postgis-partner
  "Makes the parse from domain data to database schema"
  [data]
  (-> data
      (update-in [:coverageArea]
                 #(st/multi-polygon (:coordinates %)))
      (update-in [:address]
                 #(st/point (:coordinates %)))
      (set/rename-keys {:id :external_id})
      (cp/to-case-style :snake-case)))

(defn postgis-partner->partner
  "Makes the parse from database schema to domain data.
   It will return a empty map if the param is nil."
  [postgis-data]
  (if (nil? postgis-data)
    {}
    (-> postgis-data
        (dissoc :id)
        (set/rename-keys {:external_id :id})
        (cp/to-case-style :camel-case))))
