(ns poc.api.web.routes
  (:require [reitit.swagger :as swagger]
            [poc.domain.id-generator :as gen-id]
            [poc.api.web.middlewares :as middlewares]
            [poc.api.web.swagger :as swagger-schema]
            [poc.database.repository :as repo]))

(defn get-handler [{:keys [path-params repository]}]
  (let [id   (:id path-params)
        data (repo/get-by-id repository id)]
    {:status 200
     :body   data}))

(defn get-by-location-handler [{:keys [path-params repository]}]
  (let [latitude  (:latitude path-params)
        longitude (:longitude path-params)
        data      (repo/get-by-coordinates repository latitude longitude)]
    {:status 200
     :body   data}))

(defn create-handler [request]
  (let [body    (:body-params request)
        ctx-db  (:repository request)
        partner (gen-id/add-id body)]
    (repo/insert ctx-db partner)
    {:status 201
     :body   partner}))

(def wrap-idempotency-check
  {:name ::wrap-idempotency-check
   :wrap (fn [handler]
           (fn [{:keys [body-params repository] :as request}]
             (let [id      (:id (gen-id/add-id body-params))
                   partner (repo/get-by-id repository id)]
               (if (nil? (:id partner))
                 (handler request)
                 {:status 201
                  :body   partner}))))})

(defn routes [db-component]
  [["/swagger.json"
    {:get {:no-doc  true
           :swagger {:info        {:title       "Partner REST API"
                                   :description "Swagger api to document and test calls to partners api"}
                     :definitions {:partnerRequest  swagger-schema/request-object
                                   :partnerResponse swagger-schema/response-object}}
           :handler (swagger/create-swagger-handler)}}]

   ["/partners"
    {:swagger {:tags ["Partners"]}
     :middleware [(middlewares/wrap-database db-component)
                  middlewares/wrap-unexpected-error]}
    ["" {:post {:summary    "Route to create a new partner"
                :parameters {:body swagger-schema/partner-schema}
                :responses  {201 {:body swagger-schema/partner-response}
                             400 {:body swagger-schema/bad-request-response}}
                :middleware [wrap-idempotency-check]
                :handler    create-handler}}]

    ["/:id" {:name ::get-by-id
             :get  {:summary    "Route to get a partner by id"
                    :parameters {:path swagger-schema/get-by-id-parameter}
                    :responses  {200 {:body swagger-schema/partner-response}}
                    :handler    get-handler}}]

    ["/:latitude/:longitude" {:name ::get-by-location
                              :get  {:summary    "Route to get partners by coordinates"
                                     :parameters {:path swagger-schema/get-by-coordenates}
                                     :responses  {200 {:body swagger-schema/partners-response}}
                                     :handler    get-by-location-handler}}]]])
