(ns poc.api.web.middlewares
  (:require [clojure.set :as set]
            [clojure.tools.logging :as log]))

(defn error-property [response]
  (let [body     (:body response)]
    (set/rename-keys body {:humanized :errors})))

(def wrap-rename-humanized-key-to-errors
  {:name ::wrap-rename-humanized-key-to-errors
   :wrap (fn [handler]
           (fn [request]
             (let [response (handler request)]
               (if (= 400 (:status response))
                 (assoc response :body (error-property response))
                 response))))})

(defn wrap-database [database]
  {:name ::wrap-database
   :wrap (fn [handler]
           (fn [request]
             (let [new-request (assoc request :repository database)]
               (handler new-request))))})

(defn wrap-unexpected-error [handler]
  (fn [request]
    (try
      (handler request)
      (catch Throwable e
        (log/error e)
        {:status 500
         :body {:message "internal server error"}}))))
