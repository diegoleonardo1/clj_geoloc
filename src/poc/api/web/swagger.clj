(ns poc.api.web.swagger
  (:require [poc.domain.partner :as schema]
            [malli.util :as mu]
            [malli.swagger :as swagger]
            [poc.domain.id-generator :as gen-id]))

(def partner-example
  {:tradingName  "Adega da Cerveja"
   :ownerName    "João Dias"
   :document     "85.045.071/0001-63"
   :coverageArea {:type        "MultiPolygon"
                  :coordinates [[[[30, 20], [45, 40], [10, 40], [30, 20]]],
                                [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]]}
   :address      {:type        "Point"
                  :coordinates [-46.57421, -21.785741]}})

(def partner-example-id "K6LW4S5ZKZ2M34U43KVEL7AENEQUVS2V")

(def partners-example
  [(assoc partner-example :id partner-example-id)])

(def bad-request-example
  {:errors {:coverageArea {:coordinates ["missing required key"]}
            :address      ["missing required key"]}})

(def partner-schema
  (-> (mu/dissoc schema/partner :id schema/reg)
      (mu/merge [:map {:closed true
                       :json-schema/example partner-example
                       :json-schema/closed true}])
      (mu/closed-schema schema/reg)))

(def bad-request-response
  [:map {:json-schema/example bad-request-example}
   [:errors
    [:map]]])

(def partner-response
  [:or
   (-> schema/partner
       (mu/merge [:map {:closed true
                        :json-schema/example (assoc partner-example
                                                    :id partner-example-id)
                        :json-schema/closed true}]
                 schema/reg))
   map?
   bad-request-response])

(def partners-response
  [:or
   [:vector {:json-schema/example partners-example}
    partner-response]
   vector?
   bad-request-response])

(def request-object
  (swagger/transform partner-schema schema/reg))

(def response-object
  (swagger/transform partner-response schema/reg))

(def get-by-id-parameter
  [:map
   [:id
    [:fn
     {:error/message "Invalid id format"}
     gen-id/id?]]])

(def get-by-coordenates
  [:map
   [:latitude schema/latitude]
   [:longitude schema/longitude]])
