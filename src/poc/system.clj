(ns poc.system
  (:require [poc.api.web.server :as server]
            [poc.database.postgis-repository :as repo]
            [integrant.core :as ig]))

(defn db-host [host port]
  (str host ":" port))

(defn config [profile]
  {::repo/repo
   {:mock?  (contains? #{:test nil} profile)
    :db-con {:host     (db-host (System/getenv "DB_HOST") (System/getenv "DB_PORT"))
             :user     (System/getenv "POSTGRES_USER")
             :dbname   (System/getenv "POSTGRES_DB")
             :password (System/getenv "POSTGRES_PASSWORD")}}

   ::server/server
   {:database (ig/ref ::repo/repo)
    :port     3000
    :join?    false
    :env      profile}})
