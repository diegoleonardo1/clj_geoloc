(ns poc.geoloc
  (:require [integrant.core :as ig]
            [poc.system :as system])
  (:gen-class))

(defn -main
  [& args]
  (let [env (keyword (System/getenv "APP_ENV"))]
    (ig/init (system/config env))))
