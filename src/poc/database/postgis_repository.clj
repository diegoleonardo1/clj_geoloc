(ns poc.database.postgis-repository
  (:require [clojure.java.jdbc :as jdbc]
            [clj-postgresql.core :as pg]
            [integrant.core :as ig]
            [poc.commons.data-adapter :as adapter]
            [poc.database.query-generator :as query-gen]
            [poc.database.repository :as repository]))

(defrecord partner-repository-mock [state]
  repository/IPartnerRepository

  (get-by-id [this id]
    (let [result (filter #(= (:id %) id) @(:state this))]
      (if (> (count result) 0)
        (first result)
        {})))

  (get-by-coordinates [_ _ _]
    [{:tradingName "Foo"
      :ownerName "Maradona"
      :document "12345"
      :coverageArea {:type "MultiPolygon"
                     :coordinates [[[[10 10] [10 10]]]]}
      :address {:type "Point"
                :coordinates [90 180]}}
     {:tradingName "Foo"
      :ownerName "Maradona"
      :document "12345"
      :coverageArea {:type "MultiPolygon"
                     :coordinates [[[[10 10] [10 10]]]]}
      :address {:type "Point"
                :coordinates [90 180]}}])

  (insert [this value]
    (swap! (:state this) conj value)))

(defrecord postgis-repository [db-con]
  repository/IPartnerRepository

  (get-by-id [this id]
    (-> (jdbc/query (:db-con this) (query-gen/get-by-id-query id))
        first
        adapter/postgis-partner->partner))

  (get-by-coordinates [this latitude longitude]
    (->> (jdbc/query (:db-con this) (query-gen/get-by-coordinates-query latitude longitude))
         (map adapter/postgis-partner->partner)))

  (insert  [this value]
    (jdbc/insert! (:db-con this) "partners" (adapter/partner->postgis-partner value))))

(defn create-table [db table]
  (jdbc/db-do-commands db
                       (jdbc/create-table-ddl table
                                              [[:id "serial PRIMARY KEY"]
                                               [:external_id "varchar(32) UNIQUE"]
                                               [:trading_name :text]
                                               [:owner_name :text]
                                               [:document "varchar(18) UNIQUE"]
                                               [:coverage_area "geometry(MultiPolygon,4326)"]
                                               [:address "geometry(Point,4326)"]]
                                              {:conditional? true})))

(defn postgis-extension
  "Activates the extensions to postgis"
  [db]
  (jdbc/db-do-commands db "CREATE EXTENSION IF NOT EXISTS postgis")
  (jdbc/db-do-commands db "CREATE EXTENSION IF NOT EXISTS postgis_topology"))

(defn- build-connection [{:keys [host user dbname password]}]
  (pg/pool :host host
           :user user
           :dbname dbname
           :password password))

(defn db-init
  "Initializes (activates extensions and create the table if not exist) the database"
  [connection-pool]
  (postgis-extension connection-pool)
  (create-table connection-pool :partners))

(defmethod ig/init-key ::repo
  [_ {:keys [db-con mock?]}]
  (if mock?
    (->partner-repository-mock (atom []))
    (let [connection (build-connection db-con)]
      (db-init connection)
      (->postgis-repository connection))))
