(ns poc.database.query-generator)

(defn get-by-coordinates-query
  "Builds the SQL query to retrieve data based on coordinates
   Ex:
     (get-by-coordinates-query 10 10) => [\"SELECT * FROM partners t  WHERE ST_Contains(t.coverage_area, ST_GeometryFromText(format('POINT (%1s %2s)', ?, ?), 4326))\" \"10\" \"10\"]"
  [latitude longitude]
  (let [where-clause " WHERE ST_Contains(t.coverage_area, ST_GeometryFromText(format('POINT(%1s %2s)', ?, ?), 4326))"
        sql-query    [(str "SELECT * FROM partners t " where-clause) (str latitude) (str longitude)]]
    sql-query))

(defn get-by-id-query
  "Builds the SQL query to retrieve data by external_id
   Ex:
     (get-by-id-query \"NN4VF62GC7A4AXKFUBZLGFDG72QNX6S4\") => [\"SELECT * FROM partners WHERE external_id=?\" \"NN4VF62GC7A4AXKFUBZLGFDG72QNX6S4\"]"
  [id]
  ["SELECT * FROM partners WHERE external_id=?" id])
