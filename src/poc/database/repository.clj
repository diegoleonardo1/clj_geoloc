(ns poc.database.repository)

(defprotocol IPartnerRepository
  "An abstraction to store partners"
  (get-by-id [this id] "Finds a partner by identifier")
  (get-by-coordinates [this latitude longitude] "Finds partners by latitude and longitude")
  (insert [this value] "Creates a new partner"))
