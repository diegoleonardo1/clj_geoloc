# geoloc

This is a simple Geographic information system (GIS) application that provides the possibility to create partners and search some of them based on location points that make part of their coverage area, as well as an identifier, returned in the creation process.

## Requirements

- [Docker] (https://docs.docker.com/engine/install/)
- [Docker-compose] (https://docs.docker.com/compose/install/)

## Installation

Download from https://gitlab.com/diegoleonardo1/clj_geoloc

## Usage

Run the docker-compose:

    $ cd clj_geoloc
    $ docker-compose up

## Options

There are some env vars inside the docker-compose and Dockerfile with the options that the application accepts.

The most important is "APP_ENV", which can have two values:
- prod (default)
- test (to test with a database in-memory)

## Examples

- There is a postman collection inside the "doc/postman_collection"
- After the application is running, it's possible to access the swagger API: (localhost:3000/v1/swagger).

### Bugs

- The validation inside the coordinates fields is returning the message in the wrong way when the input type is wrong.

## License

Copyright © 2021 Diego_santos
