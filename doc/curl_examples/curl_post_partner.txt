curl --request POST \
  --url http://localhost:3000/v1/partners \
  --header 'Content-Type: application/json' \
  --data '{ "tradingName": "Boteco Santo Amaro",
  "ownerName": "João Dias",
  "document": "55.991.128/0001-56",
  "coverageArea": {
    "type": "MultiPolygon",
    "coordinates": [
      [[[30, 20], [45, 40], [10, 40], [30, 20]]],
      [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
    ]
  },
  "address": {
    "type": "Point",
    "coordinates": [-46.57421, -21.785741]
  }
}'
