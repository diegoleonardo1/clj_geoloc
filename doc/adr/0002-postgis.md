Postgis\
Date: 2021-05-02

Status:\
Proposed

Context:\
Based on coordinates, It's necessary to calculate the informed latitude and longitude and find all partners available to attend that area.

Decision:\
It will use the PostGIS to make the processing of partners who will attend the region informed by the location point.

Consequences:
1. This decision will save time using an extension that already solves the problem;
2. It will use a well-documented and battle-tested solution;
3. It will help to improve the application performance;
