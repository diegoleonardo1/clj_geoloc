Project structure\
Date: 2021-05-02

Status:\
Proposed

Context:\
For the application to grow healthy, we needed a few standards to have maintainability and extensibility.

Decision:\
For simplicity's purpose, this project follows a structure more componentized, which may be very similar to the rails project's
structure.
this will make it easy to expand and maintain the application, also making the intended use of the application clear.

Consequences:
1. This change will make it easier to onboard new devs;
