FROM clojure:openjdk-14-tools-deps-slim-buster AS build

WORKDIR /app

# install cambada builder, as it will not change
RUN clj -Sdeps '{:deps {luchiniatwork/cambada {:mvn/version "1.0.0"}}}' -e :ok 

# install main deps, sometimes change
COPY deps.edn /app/deps.edn
RUN clj -e :ok

# add files and build, change often
COPY resources/ /app/resources
COPY src/ /app/src
COPY wait-for-it.sh /app/wait-for-it.sh

RUN clj -X:uberjar

FROM openjdk:14-slim-buster
WORKDIR /app

ENV APP_ENV="prod"

# set static config
EXPOSE 3000

# copy the ever changing artifact
COPY --from=build /app/geoloc.jar /app/app.jar
COPY --from=build /app/wait-for-it.sh /app/wait-for-it.sh
RUN chmod +x /app/wait-for-it.sh

# set the command, with proper container support
# CMD ["java","-XX:+UseContainerSupport","-XX:MaxRAMPercentage=85","-XX:+UnlockExperimentalVMOptions","-XX:+UseZGC","-jar","/app/app.jar"]

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
