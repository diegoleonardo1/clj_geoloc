(ns user
  (:require
   [clojure.tools.namespace.repl :as ns.repl]
   [integrant.repl :as ig.repl]
   [integrant.core :as ig]
   [poc.system :as system]))

(ig.repl/set-prep! #(ig/prep (system/config :test)))

(defn go
  ([] (ig.repl/go))
  ([ks] (ig.repl/go ks)))

(defn halt []
  (ig.repl/halt))

(defn reset []
  (ig.repl/reset))

(defn refresh []
  (ns.repl/refresh))

(defn refresh-all []
  (ns.repl/refresh-all))

(comment
  (go)

  (halt)

  (refresh-all)
  )
