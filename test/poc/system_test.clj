(ns poc.system-test
  (:require [poc.system :as sys]
            [clojure.test :refer [deftest testing is]]))

(deftest db-host
  (testing "Should return the database host correctly"
    (is (= "0.0.0.0:5432" (sys/db-host "0.0.0.0" "5432")))))
