(ns poc.commons.data-adapter-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [poc.commons.data-adapter :as adapter]))

(def partner-data
  {:id "NN4VF62GC7A4AXKFUBZLGFDG72QNX6S4",
   :tradingName "Adega da Cerveja - Santo Amaro",
   :ownerName "Diego da Silva",
   :document "1432132123891/0001",
   :coverageArea
   {:type :MultiPolygon,
    :coordinates
    [[[[30.0 20.0] [45.0 40.0] [10.0 40.0] [30.0 20.0]]]
     [[[15.0 5.0] [40.0 10.0] [10.0 20.0] [5.0 10.0] [15.0 5.0]]]]},
   :address {:type :Point, :coordinates [-46.57421 -21.785741]}})

(def postgis-data
  {:id 5,
   :external_id "NN4VF62GC7A4AXKFUBZLGFDG72QNX6S4",
   :trading_name "Adega da Cerveja - Santo Amaro",
   :owner_name "Diego da Silva",
   :document "1432132123891/0001",
   :coverage_area
   {:type :MultiPolygon,
    :coordinates
    [[[[30.0 20.0] [45.0 40.0] [10.0 40.0] [30.0 20.0]]]
     [[[15.0 5.0] [40.0 10.0] [10.0 20.0] [5.0 10.0] [15.0 5.0]]]]},
   :address {:type :Point, :coordinates [-46.57421 -21.785741]}})

(def data-matcher
  {:id           string?
   :tradingName  string?
   :ownerName    string?
   :document     string?
   :coverageArea {:type keyword?
                  :coordinates vector?}
   :address      {:type keyword?
                  :coordinates vector?}})

(defn postgis-instance? [class v]
  (instance? class v))

(def postgis-data-matcher
  {:external_id   string?
   :trading_name  string?
   :owner_name    string?
   :document      string?
   :coverage_area (partial postgis-instance? org.postgis.MultiPolygon)
   :address       (partial postgis-instance? org.postgis.Point)})

(deftest data-adapter-tests
  (testing "Should translater from postgis to domain format"
    (is (match? data-matcher
                (adapter/postgis-partner->partner postgis-data))))

  (testing "Should return a empty map if param is nil"
    (is (match? {}
                (adapter/postgis-partner->partner nil))))

  (testing "Should translater from data to postgis format"
    (is (match? postgis-data-matcher
                (adapter/partner->postgis-partner partner-data)))))
