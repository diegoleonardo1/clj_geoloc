(ns poc.commons.case-parser-test
  (:require [poc.commons.case-parser :as cp]
            [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]))

(def camelCase-matcher
  {:firstName string?
   :lastName string?})

(def snake_case-matcher
  {:first_name string?
   :last_name string?})

(def kebab-case-matcher
  {:first-name string?
   :last-name string?})

(def PascalCase-matcher
  {:FirstName string?
   :LastName string?})

(deftest to-case-style-with-simple-maps
  (testing "Should parse correctly to camelCase"
    (is (match? camelCase-matcher
                (cp/to-case-style {:first-name "John"
                                   :last-name "Doo"}
                                  :camel-case))))

  (testing "Should parse correctly to snake_case"
    (is (match? snake_case-matcher
                (cp/to-case-style {:first-name "John"
                                   :last-name "Doo"}
                                  :snake-case))))

  (testing "Should parse correctly to kebab-case"
    (is (match? kebab-case-matcher
                (cp/to-case-style {:first_name "John"
                                   :last_name "Doo"}
                                  :kebab-case))))

  (testing "Should parse correctly to PascalCase"
    (is (match? PascalCase-matcher
                (cp/to-case-style {:first-name "John"
                                   :last-name "Doo"}
                                  :pascal-case)))))

(deftest to-case-style-with-collections
  (testing "Should parse correctly to camelCase"
    (is (match? [camelCase-matcher]
                (cp/to-case-style [{:first-name "John"
                                    :last-name "Doo"}]
                                  :camel-case))))

  (testing "Should parse correctly to snake_case"
    (is (match? [snake_case-matcher]
                (cp/to-case-style [{:first-name "John"
                                    :last-name "Doo"}]
                                  :snake-case))))

  (testing "Should parse correctly to kebab-case"
    (is (match? [kebab-case-matcher]
                (cp/to-case-style [{:first_name "John"
                                    :last_name "Doo"}]
                                  :kebab-case))))

  (testing "Should parse correctly to PascalCase"
    (is (match? [PascalCase-matcher]
                (cp/to-case-style [{:first-name "John"
                                    :last-name "Doo"}]
                                  :pascal-case)))))
