(ns poc.api.web.server-test
  (:require [clojure.test :refer [deftest testing is use-fixtures]]
            [integrant.core :as ig]
            [clj-http.client :as chc]
            [poc.system :as system]
            [matcher-combinators.test :refer [match?]]
            [cheshire.core :as json]
            [poc.database.postgis-repository :as postgis-repo]))

(def service-map (system/config :test))

(defonce server-map (atom nil))

(defn start-server []
  (reset! server-map (ig/init service-map)))

(defn stop-server []
  (ig/halt! @server-map)
  (reset! server-map nil))

(defn once-fixture [tests]
  (try
    (start-server)
    (tests)
    (finally
      (stop-server))))

(use-fixtures :once once-fixture)

(def endpoint "http://localhost:3000/v1/partners")

(def post-request {:tradingName  "Foo"
                   :ownerName    "Maradona"
                   :document     "05.701.877/0001-23"
                   :coverageArea {:type        "MultiPolygon"
                                  :coordinates [[[[10 10] [10 10]]]]}
                   :address      {:type        "Point"
                                  :coordinates [90 180]}})

(defn decoded-body [response]
  (-> response
      :body
      (json/parse-string true)))

(defn inserted? [id]
  (let [state @(get-in @server-map [::postgis-repo/repo :state])
        data  (filter #(= (:id %) id) state)]
    (> (count data) 0)))

(defn post [endpoint input]
  (chc/post endpoint {:body (json/encode input)
                      :content-type :json
                      :accept :json
                      :throw-exceptions false}))

(deftest ^:integration post-partners
  (testing "Should insert a new partner"
    (let [response (post endpoint post-request)
          body     (decoded-body response)]
      (is (match? {:status 201 :body string?}
                  response))

      (is (contains? body :id))

      (is (= "6MKXOOUYH7SODDTYLKFKZ6XKI3O5M7IA"
             (:id body)))
      (is (true? (inserted? (:id body)))))))

(deftest ^:integration idempotency-post-partners
  (testing "Should have idempotency in post endpoint"
    (post endpoint post-request)
    (let [response (post endpoint post-request)
          body (decoded-body response)
          state @(get-in @server-map [::postgis-repo/repo :state])
          data (filter #(= (:id %) (:id body)) state)]
      (is (match? {:status 201} response))
      (is (true? (= (count data) 1))))))

(defn ^:integration bad-request-tests [input matcher]
  (let [response (post endpoint input)
        body     (decoded-body response)]
    (is (match? {:status 400}
                response))
    (is (match? matcher
                body))))

(defn errors-matcher [k v]
  {:errors
   {k v}})

(deftest ^:integration bad-request-average-area

  (testing "When missing coverageArea"
    (let [input (dissoc post-request :coverageArea)]
      (bad-request-tests input
                         (errors-matcher :coverageArea
                                         [string?]))))

  (testing "When missing coverageArea type property"
    (let [input (update post-request :coverageArea dissoc :type)]
      (bad-request-tests input
                         (errors-matcher :coverageArea
                                         {:type [string?]}))))

  (testing "When passing wrong value to type coverageArea property"
    (let [input (assoc-in post-request [:coverageArea :type] "WrongType")]
      (bad-request-tests input
                         (errors-matcher :coverageArea
                                         {:type ["should be MultiPolygon"]}))))

  (testing "When missing coverageArea coordinates property"
    (let [input (update post-request :coverageArea dissoc :coordinates)]
      (bad-request-tests input
                         (errors-matcher :coverageArea
                                         {:coordinates [string?]}))))

  (testing "When passing wrong latitude and longitude values to coordinates coverageArea property"
    (let [input (assoc-in post-request [:coverageArea :coordinates] [[[["" ""]]]])]
      (bad-request-tests input
                         (errors-matcher :coverageArea
                                         {:coordinates [[[[["should be a number"
                                                            "The latitude coordinate is out of range"]
                                                           ["should be a number"
                                                            "The longitude coordinate is out of range"]]]]]})))))

(deftest ^:integration bad-request-address

  (testing "When missing address property"
    (let [input (dissoc post-request :address)]
      (bad-request-tests input
                         (errors-matcher :address [string?]))))

  (testing "When missing address type property"
    (let [input (update post-request :address dissoc :type)]
      (bad-request-tests input
                         (errors-matcher :address
                                         {:type [string?]}))))

  (testing "When passing wrong value to type address property"
    (let [input (assoc-in post-request [:address :type] "WrongType")]
      (bad-request-tests input
                         (errors-matcher :address
                                         {:type ["should be Point"]}))))

  (testing "When missing address coordinates property"
    (let [input (update post-request :address dissoc :coordinates)]
      (bad-request-tests input
                         (errors-matcher :address
                                         {:coordinates [string?]}))))
  (testing "When passing wrong latitude and longitude value to coordinates address property"
    (let [input (assoc-in post-request [:address :coordinates] ["a" "b"])]
      (bad-request-tests input
                         (errors-matcher :address
                                         {:coordinates [["should be a number"
                                                         "The latitude coordinate is out of range"]
                                                        ["should be a number"
                                                         "The longitude coordinate is out of range"]]})))))
