(ns poc.api.web.routes-test
  (:require [poc.api.web.routes :as routes]
            [poc.database.postgis-repository :as postgis-repo]
            [poc.database.repository :as repo]
            [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]))

(def mock-db (postgis-repo/->partner-repository-mock (atom nil)))

(def mocked-data
  {:tradingName "Foo"
   :ownerName "Maradona"
   :document "12345"
   :coverageArea {:type "MultiPolygon"
                  :coordinates [[[[10 10] [10 10]]]]}
   :address {:type "Point"
             :coordinates [90 180]}
   :id "OJ2XRYYIDH5OII4NYRVYRECSEUQJWLDI"})

(defn sum [a b]
  (+ a b))

(def request {:path-params {:id "1"}
              :repository (reify repo/IPartnerRepository (get-by-id [_ _] :stubbed))})

(deftest get-handler
  (testing "Should return successful when the data is filled in the database"
    (let [mock (reify repo/IPartnerRepository (get-by-id [_ _]
                                               mocked-data))
          new-request (assoc request :repository mock)]
      (is (match? {:status 200
                   :body map?}
                  (routes/get-handler new-request))))))

(deftest get-by-coordinates
  (testing "Should return successful when the data is filled in the database"
    (let [mock (reify repo/IPartnerRepository (get-by-coordinates [_ _ _] [mocked-data]))
          new-request (assoc request :repository mock)]
      (is (match? {:status 200
                   :body vector?}
                  (routes/get-by-location-handler new-request))))))
