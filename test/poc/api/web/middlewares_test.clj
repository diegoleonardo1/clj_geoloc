(ns poc.api.web.middlewares-test
  (:require [poc.api.web.middlewares :as middlewares]
            [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]))

(def partner-data {:coverageArea
                   {:coordinates
                    [[[[30 20] [45 40] [10 40] [30 20]]]
                     [[[15 5] [40 10] [10 20] [5 10] [15 5]]]],
                    :type "MultiPolygon"},
                   :address {:coordinates [-46.57421 -21.785741], :type "Point"},
                   :document "1432132123891/0001",
                   :ownerName "Diego da Silva",
                   :tradingName "Adega da Cerveja - Santo Amaro",
                   :id "NN4VF62GC7A4AXKFUBZLGFDG72QNX6S4"})

(deftest unexpected-error
  (testing "Should return fail when the database throws a exception"
    (let [handler (middlewares/wrap-unexpected-error (fn [_]
                                                       (throw (Exception. "error"))))]
      (is (match? {:status 500
                   :body {:message "internal server error"}}
                  (handler partner-data))))))
