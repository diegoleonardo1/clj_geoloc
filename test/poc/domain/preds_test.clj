(ns poc.domain.preds-test
  (:require [poc.domain.preds :as preds]
            [clojure.test :refer [deftest testing is]]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [poc.domain.gen :as g]))

(deftest cpf-or-cnpj
  (testing "Should return true"
    (is (true? (preds/cpf-or-cnpj? "851.001.310-12")))
    (is (true? (preds/cpf-or-cnpj? "71.372.720/0001-26"))))

  (testing "Should return false"
    (is (false? (preds/cpf-or-cnpj? "851.001.310-400")))
    (is (false? (preds/cpf-or-cnpj? "000.000.000-00")))
    (is (false? (preds/cpf-or-cnpj? nil)))
    (is (false? (preds/cpf-or-cnpj? "")))))

(deftest lat-long-size-test
  (testing "Should return true"
    (is (true? (preds/lat-long-size? [10 10]))))
  (testing "Should return false"
    (is (false? (preds/lat-long-size? [])))
    (is (false? (preds/lat-long-size? [10])))))

(deftest in-latitude-range-test
  (testing "Should return true"
    (is (true? (preds/in-latitude-range? 10)))
    (is (true? (preds/in-latitude-range? -10)))
    (is (true? (preds/in-latitude-range? 90.000000)))
    (is (true? (preds/in-latitude-range? -90))))
  (testing "Should return false"
    (is (false? (preds/in-latitude-range? 90.000001)))
    (is (false? (preds/in-latitude-range? -90.000001)))
    (is (false? (preds/in-latitude-range? 91)))
    (is (false? (preds/in-latitude-range? -91.00000)))))

(deftest in-longitude-range-test
  (testing "Should return true"
    (is (true? (preds/in-longitude-range? 100)))
    (is (true? (preds/in-longitude-range? 100)))
    (is (true? (preds/in-longitude-range? 180)))
    (is (true? (preds/in-longitude-range? -180))))
  (testing "Should return false"
    (is (false? (preds/in-longitude-range? 180.000001)))
    (is (false? (preds/in-longitude-range? -180.000001)))
    (is (false? (preds/in-longitude-range? 181)))
    (is (false? (preds/in-longitude-range? -181)))))

(deftest non-blank
  (testing "Should return true"
    (is (true? (preds/non-blank? "foo"))))
  (testing "Should return false"
    (is (false? (preds/non-blank? "")))
    (is (false? (preds/non-blank? nil)))))

(defspec lat-long-spec 100
  (prop/for-all [p (g/gen-latitude)]
                (true? (preds/in-latitude-range? p))))

(def lat-long-spec
  (prop/for-all [p (g/gen-longitude)]
                (true? (preds/in-longitude-range? p))))
