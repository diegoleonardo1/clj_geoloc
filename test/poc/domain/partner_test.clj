(ns poc.domain.partner-test
  (:require [poc.domain.partner :as schema]
            [clojure.test :refer [deftest testing is]]
            [clojure.test.check :as tc]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [malli.generator :as mg]))

(def partner-example
  {:id            "1"
   :tradingName  "Foo Bar"
   :ownerName    "Maradona"
   :document      "90.149.373/0001-11"
   :coverageArea {:type        "MultiPolygon"
                   :coordinates [[[[10 20] [30 40]]]]}
   :address       {:type        "Point"
                   :coordinates [10 20]}})

(deftest partner-testing
  (testing "Should return true to example schema"
    (is (true? (schema/validate schema/partner partner-example))))

  (testing "Should return true if document is a valid cpf"
    (is (true? (schema/validate schema/partner
                                (assoc partner-example :document "790.161.240-12")))))

  (testing "Should return false if blank value is passed"
    (doseq [elem [:id :tradingName :ownerName :document]]
      (is (false? (schema/validate schema/partner (assoc partner-example
                                                   elem
                                                   ""))))))

  (testing "Should return false if document is nil"
    (is (false? (schema/validate schema/partner
                                 (assoc partner-example :document nil)))))

  (testing "Should return false if document is wrong format"
    (is (false? (schema/validate schema/partner
                                 (assoc partner-example :document "12345")))))

  (testing "Should return false if point coordinator is not a vector of two elements"
    (is (false? (schema/validate schema/partner (update-in partner-example
                                                     [:address :coordinates]
                                                     into
                                                     [30])))))

  (testing "Should return false if Address latitude coordinate is out of min range"
    (is (false? (schema/validate schema/partner (update-in partner-example
                                                     [:address :coordinates]
                                                     assoc
                                                     0
                                                     -91.000000)))))

  (testing "Should return false if Address latitude coordinate is out of max range"
    (is (false? (schema/validate schema/partner (update-in partner-example
                                                     [:address :coordinates]
                                                     assoc
                                                     0
                                                     91.000000)))))

  (testing "Should return false if Address longitude coordinate is out of min range"
    (is (false? (schema/validate schema/partner (update-in partner-example
                                                     [:address :coordinates]
                                                     assoc
                                                     1
                                                     -181.000000)))))

  (testing "Should return false if Address longitude coordinate is out of max range"
    (is (false? (schema/validate schema/partner (update-in partner-example
                                                     [:address :coordinates]
                                                     assoc
                                                     1
                                                     181.000000)))))

  (testing "Should return false if Multipolygon coordinates is not a vector of vector"
    (is (false? (schema/validate schema/partner (update-in partner-example
                                                     [:coverageArea :coordinates]
                                                     assoc
                                                     0
                                                     40))))))

#_(def partner-prop
    (prop/for-all [v (mg/generator schema/partner schema/reg)]
                (true? (schema/validate schema/partner v))))

#_(tc/quick-check 40 partner-prop)

#_(defspec partner-spec 40
  (prop/for-all [p (mg/generator schema/partner schema/reg)]
                (true? (schema/validate schema/partner p))))

#_(clojure.test/run-tests)

#_(mg/generate (mg/generator schema/partner schema/reg))
