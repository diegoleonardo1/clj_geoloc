(ns poc.domain.id-generator-test
  (:require [poc.domain.id-generator :as id-gen]
            [clojure.test :refer [deftest testing is]]))

(def expected-id "OJ2XRYYIDH5OII4NYRVYRECSEUQJWLDI")

(def partner {:tradingName  "Foo"
              :ownerName    "Maradona"
              :document     "12345"
              :coverageArea {:type        "MultiPolygon"
                             :coordinates [[[[10 10] [10 10]]]]}
              :address      {:type        "Point"
                             :coordinates [90 180]}})

(deftest add-id
  (testing "Should generate the right id based on map"
    (let [result (id-gen/add-id partner)]
      (is (contains? result :id))
      (is (= expected-id (:id result))))))

(deftest id?
  (testing "Should return true"
    (is (true? (id-gen/id? expected-id))))
  (testing "Should return false"
    (is (false? (id-gen/id? 1)))
    (is (false? (id-gen/id? "OJ2XRYYIDH5OII4NYRVYRECSEUQJWL10")))))

(id-gen/add-id {:id           "2",
                :tradingName  "Adega Pinheiros",
                :ownerName    "Ze da Silva",
                :document     "04.433.714/0001-44",
                :coverageArea {:type        "MultiPolygon",
                               :coordinates []},
                :address      {:type        "Point",
                               :coordinates [-49.33425,
                                             -25.380995]}})
