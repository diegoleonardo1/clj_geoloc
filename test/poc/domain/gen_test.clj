(ns poc.domain.gen-test
  (:require [poc.domain.gen :as gen]
            [poc.domain.preds :as preds]
            [clojure.test :refer [deftest testing is]]
            [clojure.test.check.generators :as generator]
            [matcher-combinators.test :refer [match?]]))

(defn generate [gen]
  (generator/generate gen))

(defn latitude-range? [v]
  (and (<= v 90)
       (>= v -90)))

(defn longitude-range? [v]
  (and (<= v 180)
       (>= v -180)))

(def lat-long-matcher
  [latitude-range? longitude-range?])

(deftest generate-latitude-value
  (testing "Should return the value between -90 and 90"
    (is (true? (-> (gen/gen-latitude)
                   generate
                   latitude-range?)))))

(deftest generate-longitude-value
  (testing "Should return the value between -180 and 180"
    (is (true? (-> (gen/gen-longitude)
                   generate
                   longitude-range?)))))

(deftest generate-vector-with-latitude-and-longitude
  (testing "Should return the vector with correct values to latitude and longitude"
    (is (match? lat-long-matcher (-> (gen/gen-lat-long)
                                     generate)))))

(deftest generate-cpf
  (testing "Should return a valid cpf"
    (is (true? (-> (gen/gen-cpf)
                   generate
                   preds/cpf?)))))

(deftest generate-cnpj
  (testing "Should return a valid cpf"
    (is (true? (-> (gen/gen-cnpj)
                   generate
                   preds/cnpj?)))))
