(ns poc.database.query-generator-test
  (:require [poc.database.query-generator :as query-gen]
            [clojure.test :refer [deftest testing is]]))

(def default-id "10")

(def query-by-id ["SELECT * FROM partners WHERE external_id=?" default-id])

(def query-by-coordinates [(str "SELECT * FROM partners t "
                                " WHERE ST_Contains(t.coverage_area, ST_GeometryFromText(format('POINT(%1s %2s)', ?, ?), 4326))") "10" "10"])

(deftest query-generators
  (testing "Should return the correct query to search by id"
    (is (= query-by-id
           (query-gen/get-by-id-query default-id))))
  (testing "Should return the correct query to search by coordinates"
    (is (= query-by-coordinates
           (query-gen/get-by-coordinates-query 10 10)))))
