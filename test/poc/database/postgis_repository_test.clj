(ns poc.database.postgis-repository-test
  (:require [poc.database.repository :as repo]
            [poc.database.postgis-repository :as postgis-repo]
            [clj-postgresql.core :as pg]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer [deftest testing is use-fixtures]]
            [expectations.clojure.test :refer [expect more->]]))

(def conn (atom nil))
(def db-name "test_conn")
(def table-name "partners")

(def partner-data {:coverageArea
                   {:coordinates
                    [[[[30 20] [45 40] [10 40] [30 20]]]
                     [[[15 5] [40 10] [10 20] [5 10] [15 5]]]],
                    :type "MultiPolygon"},
                   :address {:coordinates [-46.57421 -21.785741], :type "Point"},
                   :document "69.319.048/0001-63",
                   :ownerName "Diego da Silva",
                   :tradingName "Adega da Cerveja - Santo Amaro",
                   :id "2V7RLWE53YFGKBQL5X4JLKJM3MXQGQEY"})

(defn setup []
  (reset! conn (pg/pool :host "172.17.0.2:5432"
                        :user "postgres"
                        :dbname db-name
                        :password "mysecretpassword"))
  (postgis-repo/db-init @conn))

(defn tear-down []
  (jdbc/db-do-commands @conn [(str "DROP TABLE " table-name)])
  (pg/close! @conn)
  (reset! conn nil))

(defn fixture-db [f]
  (setup)
  (f)
  (tear-down))

(use-fixtures :each fixture-db)

(defn extension-activated? [conn]
  (->> (jdbc/query conn ["SELECT * FROM pg_extension"])
       (map :extname)
       (some #(= (or "postgis"
                     "postgis_topology")
                 %))))

(defn table-created? [conn table]
  (-> (jdbc/query conn [(str "SELECT EXISTS (SELECT FROM information_schema.tables WHERE table_name = " "'" table "'" ");")])
      first
      :exists))

(defn database-was-created? [conn table-name]
  (let [table-created (table-created? conn table-name)]
    (jdbc/db-do-commands conn [(str "DROP TABLE " table-name)])
    table-created))

(defn partner-exists? [repository partner-id]
  (let [inserted-partner (repo/get-by-id repository partner-id)]
    (is (= (:document partner-data) (:document inserted-partner)))))

(deftest ^:integration database-test
  (testing "Should activate extensions"
    (postgis-repo/postgis-extension @conn)
    (is (true? (extension-activated? @conn))))

  (testing "Should create table"
    (let [table-name "database_test"]
      (postgis-repo/create-table @conn table-name)
      (is (true? (database-was-created? @conn table-name))))))

(deftest ^:integration partner-table-test
  (let [repository (postgis-repo/->postgis-repository @conn)]
    (testing "Should create a partner"
      (repo/insert repository partner-data)
      (partner-exists? repository (:id partner-data)))))

(deftest ^:integration get-partner
  (let [repository (postgis-repo/->postgis-repository @conn)]
    (testing "Should get a partner by id"
      (let [new-partner (assoc partner-data :id "FCZVVIF4ZXMJRY66AZDPEN5EPPJWOKS4" :document "02.453.716/000170")
            insert      (repo/insert repository new-partner)
            partner     (repo/get-by-id repository (:id insert))]
        (is (not= (or {}
                      nil?)
                  partner))))))

(deftest ^:integration get-by-coordinates
  (let [repository (postgis-repo/->postgis-repository @conn)]
    (testing "Should get a partner by address coordinates"
      (let [partner (assoc partner-data :id "GLO6DTA4EDSM7EGC3UVGOEHLFLT7S7PL" :document "04.433.714/0001-44")
            _ (repo/insert repository partner)
            result (repo/get-by-coordinates repository 10 10)]
        (is (>= (count result) 1))))))

(deftest ^:integration duplicate-external-id
  (let [repository (postgis-repo/->postgis-repository @conn)]
    (testing "Should thrown exception when trying to insert a duplicated external_id"
      (repo/insert repository partner-data)
      (expect (more-> org.postgresql.util.PSQLException type
                      #"ERROR: duplicate key value violates unique constraint" ex-message)
              (repo/insert repository partner-data)))))

(deftest ^:integration duplicate-document
  (let [repository (postgis-repo/->postgis-repository @conn)]
    (testing "Should thrown exception when trying to insert a duplicated document"
      (let [data-with-repeated-document (-> partner-data
                                            (assoc :id "foo"))]
        (repo/insert repository partner-data)
        (expect (more-> org.postgresql.util.PSQLException type
                        #"ERROR: duplicate key value violates unique constraint" ex-message)
                (repo/insert repository data-with-repeated-document))))))
